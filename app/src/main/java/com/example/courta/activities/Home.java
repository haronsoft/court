package com.example.courta.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.courta.R;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
